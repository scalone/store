class AddKeys < ActiveRecord::Migration
  def change
    add_foreign_key "spree_credit_cards", "spree_payment_methods", column: "payment_method_id", name: "spree_credit_cards_payment_method_id_fk"
    add_foreign_key "spree_credit_cards", "spree_users", column: "user_id", name: "spree_credit_cards_user_id_fk"
    add_foreign_key "spree_order_promotions", "spree_promotions", column: "promotion_id", name: "spree_order_promotions_promotion_id_fk"
    add_foreign_key "spree_orders", "spree_users", column: "user_id", name: "spree_orders_user_id_fk"
    add_foreign_key "spree_payments", "spree_payment_methods", column: "payment_method_id", name: "spree_payments_payment_method_id_fk"
    add_foreign_key "spree_product_promotion_rules", "spree_promotion_rules", column: "promotion_rule_id", name: "spree_product_promotion_rules_promotion_rule_id_fk"
    add_foreign_key "spree_promotion_action_line_items", "spree_promotion_actions", column: "promotion_action_id", name: "spree_promotion_action_line_items_promotion_action_id_fk"
    add_foreign_key "spree_promotion_actions", "spree_promotions", column: "promotion_id", name: "spree_promotion_actions_promotion_id_fk"
    add_foreign_key "spree_promotion_rule_taxons", "spree_promotion_rules", column: "promotion_rule_id", name: "spree_promotion_rule_taxons_promotion_rule_id_fk"
    add_foreign_key "spree_promotion_rule_users", "spree_promotion_rules", column: "promotion_rule_id", name: "spree_promotion_rule_users_promotion_rule_id_fk"
    add_foreign_key "spree_promotion_rule_users", "spree_users", column: "user_id", name: "spree_promotion_rule_users_user_id_fk"
    add_foreign_key "spree_promotion_rules", "spree_promotions", column: "promotion_id", name: "spree_promotion_rules_promotion_id_fk"
    add_foreign_key "spree_promotion_rules", "spree_users", column: "user_id", name: "spree_promotion_rules_user_id_fk"
    add_foreign_key "spree_promotions", "spree_promotion_categories", column: "promotion_category_id", name: "spree_promotions_promotion_category_id_fk"
    add_foreign_key "spree_role_users", "spree_roles", column: "role_id", name: "spree_role_users_role_id_fk"
    add_foreign_key "spree_role_users", "spree_users", column: "user_id", name: "spree_role_users_user_id_fk"
    add_foreign_key "spree_store_credits", "spree_users", column: "user_id", name: "spree_store_credits_user_id_fk"
    add_foreign_key "spree_users", "spree_addresses", column: "bill_address_id", name: "spree_users_bill_address_id_fk"
    add_foreign_key "spree_users", "spree_addresses", column: "ship_address_id", name: "spree_users_ship_address_id_fk"
  end
end
